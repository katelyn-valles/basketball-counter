
let scoreHome = document.getElementById("score-home");
let scoreGuest = document.getElementById("score-guest");

let homeCount = 0;
let guestCount = 0;

//home functionality 

function add1HomePoint(){
    homeCount = homeCount + 1;
    scoreHome.innerText = homeCount;
}

function add2HomePoint(){
    homeCount += 2;
    scoreHome.innerText = homeCount;
}

function add3HomePoint(){
    homeCount += 3;
    scoreHome.innerText = homeCount;
}

//guest functionality 

function add1GuestPoint(){
    guestCount = guestCount + 1;
    scoreGuest.innerText = guestCount;
}

function add2GuestPoint(){
    guestCount += 2;
    scoreGuest.innerText = guestCount;
}

function add3GuestPoint(){
    guestCount += 3;
    scoreGuest.innerText = guestCount;
}
